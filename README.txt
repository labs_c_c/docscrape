############################
## Document Scraper
## Author: Cyle DeLucca
## Company: iSenpai, LLC
############################

Converts HTML, PDF, and docx to plain text for processing.

TODO: Need to make more universal/modular and get rid of text parsing unless opted for.