#!/usr/bin/python
############################
## Document Scraper
## Author: Cyle DeLucca
## Company: iSenpai, LLC
############################
# -*- coding: utf-8 -*-
#TODO Add optional paramaters to pull filenames from a list
#TODO Add option for top 10 and option to filter common words
#TODO Add help option with description of all available options

import docx2txt, itertools, operator, re, sys
from BeautifulSoup import BeautifulSoup
from cStringIO import StringIO
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfdevice import PDFDevice
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage, PDFTextExtractionNotAllowed
from pdfminer.pdfparser import PDFParser

### HTML to text *verified*
def html(html):
    try:
        data = open(html,'r').read()
        soup = BeautifulSoup(data)
    except:
        soup = BeautifulSoup(html)
    
    for script in soup(["script", "style"]):
        script.extract()
        
    text = soup.getText()
    lines = (line.strip() for line in text.splitlines())
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    text = '\n'.join(chunk for chunk in chunks if chunk)
    
    return plain_text(text)
            
### PDF to text *verified*
def pdf2text(path):
    
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = file(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos=set()
    
    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching, check_extractable=True):
        interpreter.process_page(page)
    
    text = retstr.getvalue()
    
    fp.close()
    device.close()
    retstr.close()
    return plain_text(text)

### Pull all text from docx *verified* 
def docx_to_text(docx):
    text = docx2txt.process(docx)
    return plain_text(text)
    
### Parse Text for words *verified*       
def plain_text(txt):
    count = 0
    duplicates = {}
    word_list = txt.split(' ')
    for word in word_list:
        if word.lower() in duplicates:
            duplicates[word.lower()] += 1
        elif word.lower() in filtercw:
            count += 1
        else:
            duplicates[word.lower()] = 1
    print "%d common words filtered" % count
    print "Showing top 20:"
    return sorted(duplicates.items(), key=operator.itemgetter(1), reverse=True)

### Configs and presets  
doctxt = re.compile(".*.txt$")
docpdf = re.compile(".*.pdf$")
dochtm = re.compile(".*.html$")
docx = re.compile(".*.docx$")

#input = str(sys.argv[1])

filter = open('common_words.txt', 'r')
filtercw = []
for line in filter.read().splitlines():
    filtercw.append(line)

def main():
    if doctxt.match(input):
        words = open(input, 'r')
        file_contents = words.read()
        for index, match in enumerate(plain_text(file_contents)):
            print match
            if index == 20:
                break
        #print plain_text(file_contents)
    elif docpdf.match(input):
        for index, match in enumerate(pdf2text(input)):
            print match
            if index == 20:
                break
        #print pdf2text(input)
    elif dochtm.match(input):
        for index, match in enumerate(html(input)):
            print match
            if index == 20:
                break
        #print html(input)
    elif docx.match(input):
        for index, match in enumerate(docx_to_text(input)):
            print match
            if index == 20:
                break
        #print docx_to_text(input)
    else:
        print "..I don't support that doctype yet, I do support txt, html, docx or pdf\n"
        
    
if __name__ == '__main__':
    main()